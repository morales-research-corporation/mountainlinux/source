# Sun/OS Linux 8.4
Sun/OS Linux is a free operating system that run on the base of Red Hat (Enterprise Linux) and has GNOME as it's default Desktop Environment, it brings Red Hat Enterprise Linux to the home users, developers, and PRO users.

Here are the specs:

Kernel: Linux 4.11 (SE) or MR-Linux Kernel v5.13 (optional)

Version: v8.4

Package Manager: dnf and URPM

DE (Desktop Environment): GNOME 3.38

Architecture: x86_64 (AMD and Intel)

(C) 2021 Morales Research Corporation 

