## Sun/OS v2021.02
README in French

Sun/OS est un système d'exploitation gratuit qui fonctionne sur la base d'Arch Linux et a GNOME comme environnement de bureau par défaut

Le code source est posté ici, nous avons inclus un ArchISO afin que vous construisiez Sun/OS car actuellement nous n'avons pas d'installation graphique et installons .iso. Donc, vous devez construire Sun/OS, nous avons également un installateur CLI et GUI qui vient séparément de l'ArchISO que vous pouvez ** git clone ** ... nous allons également inclure un vhd et ova pour démarrer dans virtualbox

Voici les spécifications:

Kernel: Linux 5.10.1 ou Sun v1.11.1 pour le noyau roulant

Version: v2021.02

Libération de roulement

Gestionnaire de paquet: pacman et pamac

DE (environnement de bureau): GNOME 3.38

Architecture: x86_64 (AMD et Intel)

## Sun/OS installateur v2021.02
Pour obtenir le programme d'installation de Sun/OS pour votre installation Sun/OS, accédez à ce lien pour plus d'informations et pour la source:
https://github.com/SunOS-Linux/SunOS-Installer

(C) 2021 Morales Research Corp
