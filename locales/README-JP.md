## Sun/OS v2021.02
Sun/OSは、Arch Linuxをベースに実行される無料のオペレーティングシステムであり、デフォルトのデスクトップ環境としてGNOMEを備えています。

ソースコードはここに掲載されています。現在、グラフィカルインストーラーがなく、.isoをインストールしているため、Sun/OSをビルドするためにArchISOが含まれています。 したがって、Sun/OSをビルドする必要があります。また、ArchISOとは別に付属するCLIおよびGUIインストーラーがあり、** git clone **できます... Virtualboxで起動するためのvhdとovaも含まれています

仕様は次のとおりです。

カーネル：ローリングカーネル用のLinux5.10.15または5.9

バージョン：v2021.02

ローリングリリース

パッケージマネージャー：パックマンとパマック

DE（デスクトップ環境）：GNOME 3.38

アーキテクチャ：x86_64（AMDおよびIntel）

## Sun/OS Installer v2021.02
Sun/OSインストール用のSun / OSインストーラーを入手するには、次のリンクにアクセスして詳細とソースを確認してください。
https://github.com/SunOS-Linux/SunOS-Installer

(C) 2021 Morales Research Corp
