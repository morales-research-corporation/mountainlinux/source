## Sun/OS v2021.02

README en Español 

Sun/OS es un sistema operativo gratuito que se ejecuta en la base de Arch Linux y tiene GNOME como entorno de escritorio predeterminado.

El código fuente se publica aquí, incluimos un Archiso para que usted compile Sun/OS ya que actualmente no tenemos un instalador gráfico e instalamos el .iso. Entonces tienes que compilar Sun/OS, también tenemos un instalador de CLI y GUI que viene por separado del ArchISO que puedes **git clone ** ... también incluiremos un vhd y ova para arrancar en VirtualBox

Aquí están las especificaciones:

Kernel: Linux 5.10.1 o Sun v1.11.1 para kernel rodante y estable

Versión: v2021.02

Lanzamiento rodante

Administrador de paquetes: pacman y pamac

DE (entorno de escritorio): GNOME 3.38

Arquitectura: x86_64 (AMD e Intel)

## Sun/OS Instalador v2021.02
Para obtener el instalador de Sun/OS para su instalación de Sun/OS, vaya a este enlace para obtener más información y para la fuente:
https://github.com/SunOS-Linux/SunOS-Installer

(C) 2021 Morales Research Corp
