# Sun/OS v2021.02
Sun/OS는 Arch Linux 기반에서 실행되는 무료 운영 체제이며 기본 데스크탑 환경으로 GNOME이 있습니다.

소스 코드가 여기에 게시되어 있으며 ArchISO가 포함되어 있으므로 현재 그래픽 설치 프로그램이없고 .iso를 설치하므로 Sun/OS를 빌드 할 수 있습니다. 따라서 Sun / OS를 빌드해야하며, ArchISO와 별도로 제공되는 CLI 및 GUI 설치 프로그램도 있습니다.이 설치 프로그램은 ** git clone ** ... 가상 상자에서 부팅 할 vhd 및 ova도 포함됩니다.

사양은 다음과 같습니다.

커널: 롤링 커널 용 Linux 5.10.15 또는 5.9

버전: v2021.012

롤링 릴리스

패키지 관리자 : pacman 및 pamac

DE (데스크탑 환경): GNOME 3.38

아키텍처: x86_64 (AMD 및 Intel)

## Sun/OS Installer v2021.02
Sun/OS 설치용 Sun/OS 설치 프로그램을 얻으려면이 링크로 이동하여 자세한 정보와 소스를 확인하십시오.
https://github.com/SunOS-Linux/SunOS-Installer

(C) 2021 Morales Research Corp
